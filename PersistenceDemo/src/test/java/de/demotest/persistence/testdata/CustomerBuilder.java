package de.demotest.persistence.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.demo.persistence.entity.Customer;

/**
 * Builder class for creating testdata.
 * 
 * @author S. Bleul
 * 
 */
public final class CustomerBuilder {

    private CustomerBuilder() {
    }

    /**
     * Creating a list of test devices.
     * 
     * @param amount
     *            The number of created devices.
     * @return Random devices.
     */
    public static List<Customer> createRandomAdvisors(final int amount) {
        List<Customer> result = new ArrayList<Customer>(amount);
        for (int i = 0; i < amount; i++) {
            result.add(createUniqueCustomer(i));
        }
        return result;
    }

    private static Customer createUniqueCustomer(final Integer id) {
        Customer customer = new Customer();
        customer.setCity("Munich");
        customer.setName("Advisor");
        customer.setKundennummer(new BigDecimal(id));
        return customer;
    }
}
