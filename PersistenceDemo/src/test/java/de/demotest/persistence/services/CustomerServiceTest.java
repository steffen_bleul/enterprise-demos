package de.demotest.persistence.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.demo.persistence.entity.Customer;
import de.demo.persistence.repository.CustomerRepository;
import de.demo.persistence.services.CustomerService;
import de.demotest.persistence.cfg.BasePersistenceTest;
import de.demotest.persistence.testdata.CustomerBuilder;

/**
 * @author S. Bleul
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(BasePersistenceTest.APPLICATION_CONTEXT_XML)
public class CustomerServiceTest {

    public static final int LIST_SIZE = 50;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepository customerRepo;

    private List<Customer> customers;

    /**
     * Adding database entries.
     * 
     * @throws Exception
     *             Persistence exception.
     */
    @Before
    public final void setUp() throws Exception {
        this.customers = CustomerBuilder.createRandomAdvisors(LIST_SIZE);
        this.customerRepo.save(customers); // filling database for test
    }

    /**
     * Deleting database entries.
     * 
     * @throws Exception
     *             Persistence exception.
     */
    @After
    public final void tearDown() throws Exception {
        this.customerRepo.deleteAll();
    }

    /**
     * Comparing all items.
     */
    @Test
    public final void findAllItemsTest() {
        List<Customer> results = this.customerService.findAll();
        assertTrue(results.size() == customers.size());
        for (Customer current : customers) {
            assertTrue(results.contains(current));
        }
    }

    /**
     * Count() method test.
     */
    @Test
    public final void countDevicesTest() {
        Long counter = this.customerService.count();
        assertEquals(this.customers.size(), counter.intValue());
    }

    /**
     * Testing correct paging.
     */
    @Test
    public final void pagingAllDevicesTest() {
        int pageSize = 15;
        int pages = -1;
        if (LIST_SIZE % pageSize == 0) {
            pages = (LIST_SIZE / pageSize) - 1;
        } else {
            pages = (LIST_SIZE / pageSize);
        }
        Page<Customer> results = this.customerService.findAll(new PageRequest(0, pageSize));
        while (existingAdvisorsInList(results, this.customers) && results.hasNextPage()) {
            results = this.customerService.findAll(new PageRequest(results.getNumber() + 1, pageSize));
        }
        assertTrue(pages == results.getNumber());
    }

    private boolean existingAdvisorsInList(final Iterable<Customer> foundCustomers, final List<Customer> existingCustomers) {
        for (Customer current : foundCustomers) {
            assertTrue(existingCustomers.contains(current));
        }
        return true;
    }

    /**
     * Testing correct sorting.
     */
    @Test
    public final void sortedPagingAllDevicesTest() {
        PageRequest paging = new PageRequest(0, LIST_SIZE, Direction.ASC, "city");
        this.customerService.findAll(paging);
    }

    @Test
    public void customQueryTest() {
        List<Customer> results = this.customerService.queryCustomersByParameters("Munich","Advisor");
        assertTrue(results.size() == customers.size());
    }
}
