package de.demotest.persistence.cfg;

/**
 * Base class for test fields.
 * 
 * @author S. Bleul
 * 
 */
public final class BasePersistenceTest {

    private BasePersistenceTest() {
    }

    public static final String APPLICATION_CONTEXT_XML = "classpath:testPersistenceContext.xml";
}
