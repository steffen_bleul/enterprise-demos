package de.demo.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistence entity for Advisors.
 * 
 * @author S. Bleul
 * 
 */
@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal kundennummer;

    private String name;

    private String city;

    @Column(name = "name", length = 35, nullable = true, columnDefinition = "char")
    public String getName() {
        return this.name;
    }

    public void setName(final String theName) {
        this.name = theName;
    }

    @Column(name = "city", length = 30, nullable = true, columnDefinition = "char")
    public String getCity() {
        return this.city;
    }

    public void setCity(final String theCity) {
        this.city = theCity;
    }

    @Id
    @Column(name = "ma_nr", precision = 22, nullable = true)
    public BigDecimal getKundennummer() {
        return this.kundennummer;
    }

    public void setKundennummer(final BigDecimal theKundennummer) {
        this.kundennummer = theKundennummer;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(getClass().isAssignableFrom(obj.getClass()) || obj.getClass().isAssignableFrom(getClass()))) {
            return false;
        }
        Customer other = (Customer) obj;
        if (this.getKundennummer() == null) {
            if (other.getKundennummer() != null) {
                return false;
            }
        } else if (!this.getKundennummer().equals(other.getKundennummer())) {
            return false;
        }
        return true;
    }
}
