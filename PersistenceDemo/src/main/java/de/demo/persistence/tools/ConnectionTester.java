package de.demo.persistence.tools;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.demo.persistence.repository.CustomerRepository;
import de.demo.persistence.spring.SpringContext;

/**
 * Class testing the database connection for the running persistence context.
 * 
 * @author S. Bleul
 * 
 */
public final class ConnectionTester {

    public static final Logger LOG = LoggerFactory.getLogger(ConnectionTester.class);

    private final CustomerRepository customerRepo;

    /**
     * Default constructor initializing fields.
     */
    public ConnectionTester() {
        EntityManager manager = SpringContext.getCtx().getBean(EntityManager.class);
        customerRepo = SpringContext.getCtx().getBean(CustomerRepository.class);
    }

    /**
     * Reimplementation of the connection tester. This one does not build new connections each time!
     * 
     * @return true if database could be queried.
     */
    public boolean testConnectionByInvokingQuery() {
        boolean isConnected = false;
        try {
            long counter = -1;
            counter = customerRepo.count();
            if (counter >= 0) {
                isConnected = true;
            }
        } catch (Exception e) {
            LOG.error("Database connection validation failed. The connection could not be steablished.", e);
        }
        return isConnected;
    }
}
