package de.demo.persistence.tools;

import org.hibernate.ejb.Ejb3Configuration;

/**
 * Schema Validator build on top of hibernate.
 * 
 * Uses local peristence.xml in order to connect to database.
 * 
 * If hibernate.hbm2ddl.auto property is set to "validate" the validation will be performed.
 * 
 * @author S. Bleul
 * 
 */
public final class SchemaValidator {

    private SchemaValidator() {
    }

    /**
     * Validation is triggered with man and accessible as java program.
     * 
     * @param args
     *            Arguments - not used.
     */
    public static void main(final String[] args) {
        Ejb3Configuration config = new Ejb3Configuration(); // reads persistence.xml
        config.configure("default", null);
        config.buildEntityManagerFactory(); // connects and validates
    }
}
