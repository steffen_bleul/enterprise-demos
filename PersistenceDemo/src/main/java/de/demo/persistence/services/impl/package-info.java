/**
 * This package contains service implementations.
 * <p>
 * Service implementations are automatically discovered by spring by its package name.
 *   
 */
package de.demo.persistence.services.impl;

