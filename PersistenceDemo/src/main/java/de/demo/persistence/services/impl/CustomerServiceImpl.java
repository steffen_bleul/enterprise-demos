package de.demo.persistence.services.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.demo.persistence.entity.Customer;
import de.demo.persistence.repository.CustomerRepository;
import de.demo.persistence.services.CustomerService;
import de.demo.persistence.spring.SpringContext;

/**
 * Implementation with transactions.
 * 
 * @author S. Bleul
 * 
 */
@Service
@Transactional(readOnly = true, isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED)
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepo = SpringContext.getCtx().getBean(CustomerRepository.class);

    public List<Customer> findAll() {
        return customerRepo.findAll();
    }

    public List<Customer> findAll(final Sort theSort) {
        return customerRepo.findAll(theSort);
    }

    public Page<Customer> findAll(final Pageable thePageable) {
        return customerRepo.findAll(thePageable);
    }

    public Customer findById(final BigDecimal theAdvisorId) {
        return customerRepo.findOne(theAdvisorId);
    }

    public List<Customer> findByName(final String theName) {
        return customerRepo.searchByName("%" + theName + "%");
    }

    public long count() {
        return customerRepo.count();
    }

    @Override
    public List<Customer> queryCustomersByParameters(String city, String name) {
        return customerRepo.queryCustomersByParameters(city,name);
    }

}
