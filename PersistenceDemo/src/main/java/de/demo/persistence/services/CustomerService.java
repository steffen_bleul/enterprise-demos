package de.demo.persistence.services;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import de.demo.persistence.entity.Customer;

/**
 * @author S. Bleul
 * 
 */
public interface CustomerService {

    /**
     * @return All database advisors.
     */
    List<Customer> findAll();

    /**
     * @param sort
     *            The paging information.
     * @return All database advisors.
     */
    List<Customer> findAll(Sort sort);

    /**
     * @param pageable
     *            Paging information.
     * @return All database advisors.
     */
    Page<Customer> findAll(Pageable pageable);

    /**
     * This method retrieves a single advisor.
     * 
     * @param advisorId
     *            Unique advisor Id (store).
     * @return A single advisor matching the id.
     */
    Customer findById(BigDecimal advisorId);

    /**
     * This method searches for advisors with a specific name.
     * 
     * @param name
     *            The search string.
     * @return A list of advisors matching the name.
     */
    List<Customer> findByName(String name);

    /**
     * @return The number of database entries.
     */
    long count();

    List<Customer> queryCustomersByParameters(String city, String name);
}