/**
 * This package contains services.
 * <p>
 * A service is the transaction layer in the persistence 
 * layer of the project. Repositories should be never accessed
 * directly but through a service.
 * <p>
 * Service can start transactions and leaving a services closes
 * a database transaction. In order to program batch jobs on
 * entity operations do this always inside the services.
 * <p>
 * Services are instantiated by Spring at runtime, check the
 * component-scan configuration inside your spring context
 * configuration. Services are discovered dynamically by Spring.
 *   
 */
package de.demo.persistence.services;

