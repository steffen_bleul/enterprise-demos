package de.demo.persistence.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author S. Bleul
 * 
 */
public final class SpringContext implements ApplicationContextAware {

    private SpringContext() {
    }

    private static ApplicationContext ctx;

    private synchronized void setCtx(final ApplicationContext ctx) {
        SpringContext.ctx = ctx;
    }

    /**
     * @return The spring application context.
     */
    public static ApplicationContext getCtx() {
        return ctx;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(final ApplicationContext theApplicationContext) throws BeansException {
        this.setCtx(theApplicationContext);
    }
}
