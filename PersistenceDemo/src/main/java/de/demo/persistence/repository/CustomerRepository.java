package de.demo.persistence.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.demo.persistence.entity.Customer;

/**
 * Spring JPA-Data Repository.
 * 
 * @author S. Bleul
 * 
 */
public interface CustomerRepository extends JpaRepository<Customer, BigDecimal>, CustomerRepositoryCustom {

    /**
     * Search advisors by name. Name is concatenated by SQL-Like operator.
     * 
     * @param name
     *            A name or sub-string of a name.
     * @return Matching advisors.
     */
    @Query("select a from Customer a where rtrim(name) like ?1")
    List<Customer> searchByName(String name);
}
