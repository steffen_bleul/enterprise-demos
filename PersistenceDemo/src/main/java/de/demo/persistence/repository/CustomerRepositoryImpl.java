package de.demo.persistence.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import de.demo.persistence.entity.Customer;
import de.demo.persistence.spring.SpringContext;

/**
 * Customized queries for the order table.
 * 
 * @author S. Bleul
 * 
 */
public class CustomerRepositoryImpl implements CustomerRepositoryCustom {

    public List<Customer> queryCustomersByParameters(final String theCity, final String theName) {
        EntityManager em = SpringContext.getCtx().getBean("entityManager", EntityManager.class);
        String query = "select a from Customer a where rtrim(name) like ?1 and rtrim(city) like ?2";
        Query q = em.createQuery(query);
        q.setParameter(1, theName).setParameter(2, theCity);
        return q.getResultList();
    }

}
