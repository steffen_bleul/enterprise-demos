package de.demo.persistence.repository;

import java.util.List;

import de.demo.persistence.entity.Customer;

/**
 * Custom, dynamic built queries for the order table.
 * 
 * @author S. Bleul
 * 
 */
public interface CustomerRepositoryCustom {

    /**
     * Dynamic query where the search is defined by a parameters object.
     * 
     * @param parameters
     *            Containing the search parameters.
     * @return The search result.
     */
    List<Customer> queryCustomersByParameters(String city, String name);
}
