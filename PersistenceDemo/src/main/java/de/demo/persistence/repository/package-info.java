/**
 * This package contains Repositories
 * <p>
 * Entities are simple bean representations of database
 * tables. The table access should be implemented elsewhere.
 * This is done by a Repository. Here custom queries are declared and
 * implemented.
 * <p>
 * Here we introduce GenericRepositories, this means, they already implement
 * basic access operations for the database. The declared Repositories are
 * implemented using the JPA-DATA Spring framework. Already coming
 * with solutions for paging, sorting and customizing queries.
 */
package de.demo.persistence.repository;

